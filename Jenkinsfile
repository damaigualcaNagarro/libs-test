pipeline {
  environment {
    imageName = 't24-app-r21:R21.619'     //path project gitlab
    gitlabCredential = 'gitlabToken'
    argoCDFolderApp = 'carton-plast/microservices/cp-core-ws'     //path argocd
    dockerImage = ''
  }

  agent {
    kubernetes {
      yaml '''
        apiVersion: v1
        kind: Pod
        spec:
          containers:
          - name: dind
            image: docker:19.03.1-dind
            securityContext:
              privileged: true
            env:
              - name: DOCKER_TLS_CERTDIR
                value: ""
          - name: kustomize
            image: k8s.gcr.io/kustomize/kustomize:v4.1.3
            command:
            - cat
            tty: true
          - name: gradle
            image: gradle:6.8.3-jdk11
            command:
            - cat
            resources:
              requests:
                memory: "2Gi"
                cpu: "2"
              limits:
                memory: "6Gi"
                cpu: "2"
            tty: true
        '''
    }
  }
  stages {
      stage('Run Tests') {
          steps {
              container('gradle') {
                  sh 'gradle -version'
                  echo 'Running tests..'
              }
          }
      }
      stage('SonarQube Analysis') {
          when {
              not {
                  expression { BRANCH_NAME ==~ /(main|develop)/ }
              }
          }
          steps {
              withSonarQubeEnv('sonar') {
                  container('gradle') {
                      echo 'SonarQube Analysis project..'
                  }
              }
          }
      }
      stage('Build docker image') {
          when {
              expression { BRANCH_NAME ==~ /(main|develop)/ }
          }
          steps {
              container('dind') {
                  echo 'Building docker image..'
                  script {
                      dockerImage = docker.build + imageName + ":${env.GIT_COMMIT}"
                  }
              }
          }
      }
      stage('Deploy project to develop') {
          when {
            expression { BRANCH_NAME ==~ /(develop)/ }
          }
          steps {
              container('kustomize') {
                  echo 'Deploying to test environment..'
                  checkout([$class: 'GitSCM',
                      branches: [[name: '*/main' ]],
                      extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: 'argocd-test-apps'], [$class: 'ScmName', name: 'argocd-test-apps']],
                      userRemoteConfigs: [[
                          url: 'https://gitlab.crsoft.org/devops/kubernetes/argocd-test-apps.git',
                          credentialsId: gitlabCredential
                      ]]
                  ])
                  dir('./argocd-test-apps/' + argoCDFolderApp + '/settings' ) {
                      sh('kustomize edit set image ' + env.REGISTRY + imageName + ':' + env.GIT_COMMIT)
                      sh('git config --global user.email jenkinsci@ci.com')
                      sh('git config --global user.name Jenkins Pipeline')
                      sh "git commit -am 'Publish new version ${argoCDFolderApp}'"
                      withCredentials([usernamePassword(credentialsId: gitlabCredential, passwordVariable: 'GIT_PASSWORD', usernameVariable: 'GIT_USERNAME')]) {
                          sh "git push https://${GIT_USERNAME}:${GIT_PASSWORD}@${env.ARGO_APPS_TEST} HEAD:main || echo 'no changes'"
                      }
                  }
              }
          }
      }
  }
}